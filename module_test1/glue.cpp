#include "glue.hpp"
#include <string> 
#include <cstring>


MATSEL void CreateModule(CreateModuleArgs* args)
{
    args->name = AllocCString("Test Module #1");
    args->description = AllocCString("Description of test module 1. Lots of test stuff. ddlll");

    args->schema = new Schema;
    args->schema->numLabels = 2;
    
    args->schema->types = new SchemaType[args->schema->numLabels];
    args->schema->types[0] = SchemaType::String;
    args->schema->types[1] = SchemaType::Real;

    args->schema->labels = new char* [args->schema->numLabels];
    args->schema->labels[0] = AllocCString("TestProp1");
    args->schema->labels[1] = AllocCString("AnotherProp");
}

MATSEL void DestroyModule()
{

}

MATSEL void SubmitData(void* data, size_t sz)
{
    
}