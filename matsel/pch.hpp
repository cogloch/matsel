#pragma once


#pragma warning(push, 0)

// Windows
//#ifdef _DEBUG
//#define _AFX_NO_DEBUG_CRT 1 
//#endif
#define _WIN32_WINNT 0x0601

#include <afxwin.h>      
#include <afxdlgs.h>          
#include <afxpriv.h>
#include <ImageHlp.h>     
#include <afxcview.h>    

// Standard 
#include <map>
#include <ctime>
#include <array>
#include <string>
#include <memory>             
#include <vector>
#include <chrono>
#include <fstream>
#include <cstdint>
#include <optional>
#include <filesystem>
#include <functional>

// SQLite
#undef max
#include <sqlite_modern_cpp.h> 

// Other .lib s not handled by the ide/vcpkg/nuget/whatever
#pragma comment(lib, "imagehlp.lib")


#pragma warning(pop)