#pragma once
#include <vector>
#include <string>


#define FREE_API

// Assume for now that modules will not export any other symbols than the ones defined in the API (if they do, ignore them)
// VS will define by default $(PROJECTNAME)_EXPORTS
// It's ugly, just require #define MODULE wherever the API is being used in the module; the core will never include module headers
#ifdef FREE_API      // API is made up of only free functions 
#ifdef MODULE
#define MATSEL extern "C" __declspec(dllexport)
#else
#define MATSEL extern "C" __declspec(dllimport)
#endif
#else                // API uses classes 
#ifdef MODULE
#define MATSEL __declspec(dllexport)
#else
#define MATSEL __declspec(dllimport)
#endif
#endif

// C linkage for FREE_API?
//struct Metadata
//{
//    const char* name;              // Will be used in dependencies 
//    const char* description;       // Purely for human niceness 
//};

inline char* AllocCString(const std::string& source)
{
    const size_t sz = source.size() + 1; // + \0
    char* ret = new char[source.size() + 1]; 
    strcpy_s(ret, sz, source.c_str());
    return ret;
}

inline wchar_t* AllocCWString(const std::wstring& source)
{
    const size_t sz = source.size() + 1; // + \0
    wchar_t* ret = new wchar_t[sz]; 
    wcscpy_s(ret, sz, source.c_str());
    return ret;
}

inline void FreeCString(char** arr)
{
    if (*arr)
    {
        delete[] *arr;
        *arr = nullptr;
    }
}

inline void FreeCWString(wchar_t** arr)
{
    if (*arr)
    {
        delete[] *arr;
        *arr = nullptr;
    }
}

// Inter- module/db links that can be requested/stored by modules from other modules at runtime:
//            requesting module (sync req)-> core queue (sync target)-> target module (sync target)-> core queue (sync req)-> requesting module
// For now, simply pass around a copy
// TODO: do the whole buffering thing from the spec 
// TODO: working with the labels will become significantly heavy at some point; rethink how references are stored (hash module/db names?)   
//                   ---> at least the column/field could be indexed at this point (but not doing it yet) 
// TODO: tag priority 
struct DataReference
{
    enum class Type
    {
        None = 0,
        Submission,       // Module is forwarding some data to the core (hence DB)
        Request           // Module is requesting some data from the core (hence DB) 
    } type;

    wchar_t* mod      = nullptr;
    wchar_t* db       = nullptr;
    wchar_t* field    = nullptr;
    std::int64_t key       = -1;

    wchar_t* value    = nullptr;
};

inline DataReference BuildDataRef(DataReference::Type type, const std::wstring& mod, const std::wstring& db, const std::wstring& field, std::int64_t key)
{
    DataReference ref;
    ref.type = type;

    ref.mod = AllocCWString(mod);
    ref.db = AllocCWString(db);
    ref.field = AllocCWString(field);

    ref.key = key;

    return ref;
}

inline void CleanupDataReference(DataReference& ref)
{
    FreeCWString(&ref.value);
    FreeCWString(&ref.mod);
    FreeCWString(&ref.db);
    FreeCWString(&ref.field);

    ref.key = -1;
}

enum class SchemaType
{
    // u8, u16, u32, u64,
    // i8, i16, i32, i64,
    None = 0,
    Integer,    // signed 64-bit int (i64)
    String,     // utf-8 (8 bit char)
    Real        // double (f64)
};

// Does not include special/common columns (id/key - int, source - string, module - string (?))
struct Schema
{
    size_t      numLabels    = 0;
    char**      labels       = nullptr;
    SchemaType* types        = nullptr;
};

struct CreateModuleArgs
{
    char*   name           = nullptr;
    char*   description    = nullptr;
    Schema* schema         = nullptr;
};

// TODO: non-blocking reference requests (to be fulfilled at the next sync)
// TODO: reference queries API 

// const char*, const char*, const char**, size_t*, SchemaType*, size_t
MATSEL void CreateModule(CreateModuleArgs*);
MATSEL void DestroyModule();
MATSEL void SubmitData(void* data, size_t sz);
// Get data from db by some id
// Batch data 
// Request data by module to core (hence another module)
// Way of submitting data to the module, from the core (AcceptData(data,sz),etc.)
// References as module_name:ID?
// Get ID by some other piece of the schema (i.e. "search")

// Entry point for every module (any preparation that needs to be done before this point can be handled in dllmain)
//class Module
//{
//public:
//    virtual ~Module() = default;
//
//    // Will pass exported symbols, required depedencies, and metadata to the core 
//    // Keep pure abstract for now
//    //void Init();
//
//    // Will create the .def file if not already existing (or will overwrite if it's not corresponding to the list returned here)
//    // Called once at initialisation in ::Init()
//    virtual const std::vector<std::string> GetExportedSymbols() const = 0;
//
//    // Not changing beyond loading
//    struct Metadata
//    {
//    };
//    virtual Metadata GetMetadata() = 0;
//
//    virtual void Tick() = 0;
//};

//__declspec(dllexport) Module* CreateModule();
//__declspec(dllexport) void DestroyModule(Module*);