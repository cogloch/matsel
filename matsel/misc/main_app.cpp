#include "pch.hpp"
#include "main_app.hpp"
#include "windows/dummy_dialog.hpp"
#include "windows/module_browser.hpp"    // Module browser dialog


namespace cuc
{ 

BOOL MainApp::InitInstance()
{
    CWinApp::InitInstance();

    // "Parent" for main dialogs 
    auto dummy = new DummyDialog;
    m_pMainWnd = dummy;
    dummy->Launch();

    // Actual GUI
    auto dlg = new ModuleBrowser;
    dlg->Launch();
    
    return true;
}

} // namespace cuc