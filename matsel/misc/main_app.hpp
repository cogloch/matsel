#pragma once


namespace cuc
{ 

class MainApp : public CWinApp
{
public:
    MainApp() = default;
    BOOL InitInstance() override;
};

} // namespace cuc