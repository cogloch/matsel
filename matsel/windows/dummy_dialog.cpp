#include "pch.hpp"
#include "dummy_dialog.hpp"


namespace cuc
{

BOOL DummyDialog::OnInitDialog()
{
    CDialog::OnInitDialog();

    MoveWindow(0, 0, 0, 0);
    EnableWindow(false);

    return true;
}

} // namespace cuc