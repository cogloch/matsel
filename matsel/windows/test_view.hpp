#pragma once


namespace cuc
{

class TestView : public CWnd
{
public:
	CD2DTextFormat* textFormat;
	CD2DSolidColorBrush* blueBrush;

	TestView();
	LRESULT OnDraw2d(WPARAM, LPARAM);

	DECLARE_DYNCREATE(TestView)
	DECLARE_MESSAGE_MAP()
};



class TestFrame : public CFrameWnd
{
public:
	TestView view;

	TestFrame();
	int OnCreate(CREATESTRUCT*);
	void OnSetFocus(CWnd*);
	BOOL OnCmdMsg(UINT, int, void*, AFX_CMDHANDLERINFO*);

	DECLARE_DYNAMIC(TestFrame)
	DECLARE_MESSAGE_MAP()
};

} // namespace cuc 