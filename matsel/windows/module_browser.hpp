#pragma once
#include "gui/mfcpp.hpp"    
#include "gui/dialog.hpp"
#include "db_browser.hpp"
#include "gui/edit_control.hpp"
#include "gui/button_control.hpp"
#include "gui/list_box_control.hpp"


// TODO: Fix module updating

namespace cuc
{

class ModuleBrowser : public ModelessDialog
{
public:
    ModuleBrowser();

private:
    enum Button
    {
        AddMod, UpdateMod, RemoveMod,
        ViewDb, OpenDb, CloseDb, CreateDb, DeleteDb,
        BtnCount
    };
    std::array<PushButton, BtnCount> m_buttons;

    enum ControlGroup
    {
        Modules, Databases,
        GroupCount
    };
    std::array<GroupBox, GroupCount>       m_groups;
    std::array<ListBoxControl, GroupCount> m_lists;
    std::array<EditControl, GroupCount>    m_metadataPanels;
};

} // namespace cuc