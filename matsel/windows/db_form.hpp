#pragma once
#include "gui/dialog.hpp"
#include "module_system/module_descriptor.hpp"
#include "gui/edit_control.hpp"
#include "gui/button_control.hpp"


namespace cuc
{

class DbForm : public ModalDialog
{
	DECLARE_MESSAGE_MAP()

public:
    // Input reference format:
    //   !<module_name>.<db_name>.<key>
    // TODO: array of refs: if multiple ! are found, each piece of input following ! will be treated as a different reference 
    //            --> !<module_name>.<db_name>.<col>.<key 1>!<module_name>.<db_name>.<col>.<key 2>
    //            --> for now, items in ref lists differing only by key, and share the module/db/col 
    // TODO: drag and drop 
    //   between db views (create ref at target) 
    // TODO: cook and reuse layouts 
    DbForm(std::vector<std::wstring>& fields, CoreSchema& schema);
    ~DbForm();

    std::vector<std::wstring>& m_fields;
    CoreSchema& m_schema;

	void OnDestroy();

private:
    std::vector<CStatic*> labels;
    std::vector<EditControl*> editBoxes;

    PushButton* m_okBtn;
    PushButton* m_cancelBtn;
};

} // namespace cuc 