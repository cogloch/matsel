#include "pch.hpp"
#include "test_view.hpp"


namespace cuc
{

IMPLEMENT_DYNCREATE(TestView, CWnd)
BEGIN_MESSAGE_MAP(TestView, CWnd)
	ON_REGISTERED_MESSAGE(AFX_WM_DRAW2D, &TestView::OnDraw2d)
END_MESSAGE_MAP()

TestView::TestView()
{
	EnableD2DSupport();

	blueBrush = new CD2DSolidColorBrush(GetRenderTarget(), D2D1::ColorF(D2D1::ColorF::RoyalBlue));
	textFormat = new CD2DTextFormat(GetRenderTarget(), L"Gabriola", 50);
	textFormat->Get()->SetTextAlignment(DWRITE_TEXT_ALIGNMENT_CENTER);
	textFormat->Get()->SetParagraphAlignment(DWRITE_PARAGRAPH_ALIGNMENT_CENTER);
}

LRESULT TestView::OnDraw2d(WPARAM, LPARAM lparam)
{
	auto target = (CHwndRenderTarget*)lparam;

	target->Clear(D2D1::ColorF(D2D1::ColorF::Beige));

	CRect rect;
	GetClientRect(rect);
	target->DrawTextW(L"Test", rect, blueBrush, textFormat);

	return true;
}

IMPLEMENT_DYNAMIC(TestFrame, CFrameWnd)
BEGIN_MESSAGE_MAP(TestFrame, CFrameWnd)
	ON_WM_CREATE()
	ON_WM_SETFOCUS()
END_MESSAGE_MAP()

TestFrame::TestFrame()
{
	Create(nullptr, L"View");
}

int TestFrame::OnCreate(CREATESTRUCT* cstruct)
{
	CFrameWnd::OnCreate(cstruct);
	view.Create(nullptr, nullptr, AFX_WS_DEFAULT_VIEW, {}, this, AFX_IDW_PANE_FIRST, nullptr);
	return 0;
}

void TestFrame::OnSetFocus(CWnd*)
{
	view.SetFocus();
}

BOOL TestFrame::OnCmdMsg(UINT id, int code, void* extra, AFX_CMDHANDLERINFO* handlerInfo)
{
	if (view.OnCmdMsg(id, code, extra, handlerInfo))
		return true;
	return CFrameWnd::OnCmdMsg(id, code, extra, handlerInfo);
}

} // namespace cuc 