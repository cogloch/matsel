#pragma once
#include "gui/dialog.hpp"
#include "gui/list_control.hpp"
#include "gui/button_control.hpp"
#include "module_system/module_manager.hpp"
#include "module_system/module_descriptor.hpp"


namespace cuc
{ 

class DbInstance;

class DbBrowserDialog : public ModelessDialog
{
public:
    DbBrowserDialog(DbInstance&, const ModuleManager&);
    void RefreshData();

private:
    DbInstance& m_db;
    const ModuleManager& m_moduleManager;    // Only for solving inter- module/db references
    
    // Controls 
    ListGridControl m_list;

    CStatic m_note;        // TODO: temp
    std::array<PushButton, 4> m_buttons;
    enum BtnIndex { Insert, Update, Delete, Import };

    void ResizeDialog(int minWidth);
};

} // namespace cuc