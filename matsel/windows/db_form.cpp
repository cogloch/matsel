#include "pch.hpp"
#include "db_form.hpp"


namespace cuc 
{

BEGIN_MESSAGE_MAP(DbForm, ModalDialog)
	ON_WM_DESTROY()
END_MESSAGE_MAP()


void DbForm::OnDestroy()
{
	for (size_t i = 0; i < m_fields.size(); ++i)
	{
		CString val;
		GetDlgItemTextW(editBoxes[i]->GetId(), val);
		m_fields[i] = val.GetString();
	}
	ModalDialog::OnDestroy();
}

DbForm::DbForm(std::vector<std::wstring>& fields, CoreSchema& schema)
    : m_fields(fields)
    , m_schema(schema)
{
    m_initLayoutProc = [this]() {
        int y = 5;
        const size_t numFields = m_schema.size();
        m_fields.resize(numFields);
        
        for (size_t i = 0; i < numFields; ++i)
        {
            std::wstring label = ((CStringW)(m_schema[i].first.c_str())).GetString();
            auto sep = label.find(L"$");
            if (sep != std::string::npos) label = label.erase(sep);
            labels.push_back(new CStatic); // TODO: yikes
            labels[i]->Create(label.c_str(), WS_CHILD | WS_VISIBLE, { 20, y, 120, y + 20 }, this);
            labels[i]->SetFont(GetFont());
            
            editBoxes.push_back(new EditControl); // TODO: yikes
            //editBoxes[i]->Create(WS_CHILD | WS_VISIBLE | WS_TABSTOP, { 130, y, 210, y + 20 }, this, );
            editBoxes[i]->Create(EditControl::Style::Left, GuiControl::Style::Child | GuiControl::Style::Visible | GuiControl::Style::TabStop,
                { 130, y, 210, y + 20 }, this);

            y += 25;
        }

        m_okBtn = new PushButton();
        m_okBtn->Create(L"Ok", { 20, y, 210, y + 20 }, this);
        m_okBtn->SetHandler([this]() {
            for (size_t i = 0; i < m_fields.size(); ++i)
            {
                CString val;
                GetDlgItemTextW(editBoxes[i]->GetId(), val);
                m_fields[i] = val.GetString();
            }

            CDialog::EndDialog(IDOK);
        });
        
        y += 25;

        m_cancelBtn = new PushButton();
        m_cancelBtn->Create(L"Cancel", { 20, y, 210, y + 20 }, this);
        m_cancelBtn->SetHandler([this]() {
            CDialog::EndDialog(IDCANCEL);
        });

        MoveWindow(0, 0, 250, y + 90);
    };

    m_initDataProc = [this]() {
        for (size_t fieldIdx = 0; fieldIdx < m_fields.size(); ++fieldIdx)
        {
            editBoxes[fieldIdx]->SetWindowTextW(m_fields[fieldIdx].c_str());
        }
    };
}

DbForm::~DbForm()
{
    for (auto& label : labels) delete label;
    for (auto& ebox : editBoxes) delete ebox;
    delete m_okBtn;
    delete m_cancelBtn;
}

} // namespace cuc