#include "pch.hpp"
#include "db_browser.hpp"
#include "db_form.hpp"
#include "module_system/db_instance.hpp"

#include "test_view.hpp"


namespace cuc
{

std::vector<RECT> GetBtnRowExtents(int numBtns, int x, int y, int maxWidth, int xSep = 5, int height = 20)
{
    std::vector<RECT> rects(numBtns);
    if (numBtns < 1) return rects;
    
    int width = (maxWidth - (numBtns - 1) * xSep) / numBtns;

    rects[0].left = x;
    rects[0].top = y;
    rects[0].right = x + width;
    rects[0].bottom = y + height;

    for (int i = 1; i < numBtns; ++i)
        rects[i] = { rects[i - 1].right + xSep, rects[i - 1].top, rects[i - 1].right + xSep + width, rects[i - 1].bottom };
    return rects;
}

DbBrowserDialog::DbBrowserDialog(DbInstance& db, const ModuleManager& modMgr) : m_db(db), m_moduleManager(modMgr)
{
    m_initLayoutProc = [this]() {
        SetWindowTextW((L"Browse DB: " + m_db.GetName()).c_str());

        m_list.Create({}, this);
        m_list.AddColumn("id");
        auto schema = m_db.m_coreSchema;
        for (auto& col : schema)
        {
            auto sep = col.first.find("$");
            std::string label = col.first;
            if (sep != std::string::npos) label = label.erase(sep);
            m_list.AddColumn(label);
        }

        m_list.SetHandlerSelect([this]() {
            m_buttons[Update].Enable();
            m_buttons[Delete].Enable();
        });

        m_list.SetHandlerDblClick([this]() {
            auto test = new TestFrame;
            test->ShowWindow(SW_SHOW);
            test->UpdateWindow();
        });

        auto rects = GetBtnRowExtents(4, 10, 5, 400);
        std::wstring btnLabels[4] = { L"Insert", L"Update", L"Delete", L"Import" };
        for (int i = 0; i < 4; ++i)
            m_buttons[i].Create(btnLabels[i], rects[i], this);

        m_buttons[Insert].SetHandler([this]() {
            std::vector<std::wstring> output;
            auto specificSchema = m_db.m_coreSchema;
            specificSchema.pop_back(); specificSchema.pop_back(); specificSchema.pop_back(); // Fields only for specific cols
            DbForm form(output, specificSchema);
            if (form.Launch() == IDOK)
            {
                // TODO: validate 

                // TODO: remove special fields from db form 
                std::vector<std::string> outputA;
                for (auto& col : output)
                    outputA.push_back(((CStringA)(col.c_str())).GetString());

                // Special cols 
                outputA.push_back("manual");

                std::time_t result = std::time(nullptr);
                std::string timestamp(std::asctime(std::localtime(&result))); // wall = "sim"
                outputA.push_back(timestamp);
                outputA.push_back(timestamp);
                
                m_db.InsertRow(outputA);

                RefreshData();
            }
        });

        m_buttons[Update].Disable();
        m_buttons[Update].SetHandler([this]() {
            const int selRow = m_list.GetSelectedRow();
            auto output = m_list.GetRowData(selRow);

            auto specificSchema = m_db.m_coreSchema;
            specificSchema.pop_back(); specificSchema.pop_back(); specificSchema.pop_back(); // Fields only for specific cols
            output.pop_back(); output.pop_back(); output.pop_back();
            DbForm form(output, specificSchema);
            if (form.Launch() == IDOK)
            {
                // TODO: validate 

                std::vector<std::string> outputA;
                for (auto& col : output)
                    outputA.push_back(((CStringA)(col.c_str())).GetString());

                // Special cols 
                outputA.push_back("manual");

                std::time_t result = std::time(nullptr);
                std::string timestamp(std::asctime(std::localtime(&result))); // wall = "sim"
                outputA.push_back(timestamp);
                outputA.push_back(timestamp);

                m_db.UpdateRow(m_list.GetRowKey(selRow), outputA);

                RefreshData();
            }

            RefreshData();
        });

        m_buttons[Delete].Disable();
        m_buttons[Delete].SetHandler([this]() {
            i64 key = m_list.GetRowKey(m_list.GetSelectedRow());
            //m_db.NullifyRow(key);
            m_db.DeleteRow(key);

            m_list.Deselect();
            m_buttons[Update].Disable();
            m_buttons[Delete].Disable();

            RefreshData();
        });

        m_buttons[Import].SetHandler([this]() {
            RefreshData();
        });

        int noteWidth = 200;
        m_note.Create(L"TODO: Actual toolbar", WS_CHILD | WS_VISIBLE, { rects[Import].right + 5, 5, rects[Import].right + 5 + noteWidth, 25 }, this);
        
        // Accelerators 
        m_list.SetHandlerKeyUp([this](UINT key) {
            switch (key)
            {
            case 'I': m_buttons[Insert].SendMessage(WM_LBUTTONUP); break; // 0x49
            case 'U': m_buttons[Update].SendMessage(WM_LBUTTONUP); break;
            case 'D': m_buttons[Delete].SendMessage(WM_LBUTTONUP); break;
            }
        });

        ResizeDialog(rects[Import].right + 10 + noteWidth);
    };

    m_initDataProc = [this]() {
        RefreshData();
    };
}

void InsertRow(ListGridControl& list, int id, std::vector<std::string> data)
{
    int row = list.InsertItem(0, std::to_wstring(id).c_str());
    for (size_t col = 1; col <= data.size(); ++col)
        list.SetItemText(row, col, AtoW(data[col - 1]).c_str());
}

// TODO: cache; otherwise, when there will be lots of inserts/updates (with synchronization), everything would just die  
void DbBrowserDialog::RefreshData()
{
    m_list.DeleteAllItems();

    std::vector<std::vector<std::string>> data;
    auto ids = m_db.GetNonNullKeys();

    data.resize(ids.size());

    for (size_t col = 0; col < m_db.m_coreSchema.size(); ++col)
    {
        int row = 0;
        m_db.Get() << "select " + m_db.m_coreSchema.at(col).first + " from mat;" >> [&](std::string ret) {
            // TODO: test references; only one ref per field
            //std::vector<size_t> refSepPos; // position of separator, i.e. inclusive
            std::wstring solved;
            int lastPos = -1;
            for(;;)
            {
                std::wstring tempToken;

                // TODO: separate util for decoding refs (and ref arrays) as strings
                // TODO: jesus 
                lastPos = ret.find("!", lastPos + 1);
                if (lastPos == std::string::npos)
                    break;
                
                size_t pos = ret.find(".", lastPos + 1);
                if (pos == std::string::npos)
                    continue;
                std::wstring srcMod = AtoW(ret.substr(lastPos + 1, pos - lastPos - 1));
                lastPos = pos;
                tempToken += L"!" + srcMod;

                pos = ret.find(".", lastPos + 1);
                if (pos == std::string::npos)
                    continue;
                std::wstring srcDb = AtoW(ret.substr(lastPos + 1, pos - lastPos - 1));
                lastPos = pos;
                tempToken += L"." + srcDb;

                // TODO: solve type identifier: col$type
                pos = ret.find(".", lastPos + 1);
                if (pos == std::string::npos)
                    continue;
                std::wstring srcCol = AtoW(ret.substr(lastPos + 1, pos - lastPos - 1));
                lastPos = pos;
                tempToken += L"." + srcCol;

                /*static const std::map<SchemaType, std::string> typeMap = {
                    { SchemaType::String, "text" },
                    { SchemaType::Integer, "int" },
                    { SchemaType::Real, "real" },
                };

                for (auto& col : m_db.m_coreSchema)
                    if (col.first == )
                    col.first += "$" + typeMap.at(col.second);*/

                pos = ret.find_first_of(" !,", lastPos + 1);
                std::wstring key = AtoW(ret.substr(lastPos + 1, pos - lastPos - 1));
                tempToken += L"." + key;

                auto ref = BuildDataRef(DataReference::Type::Request, srcMod, srcDb, srcCol, std::stoi(key));
                m_moduleManager.SolveReference(ref);
                
                if (ref.value)
                    solved += std::wstring(ref.value);
                else
                    solved += tempToken;
            }

            if (solved.empty())
                solved = AtoW(ret);

            data[row].push_back(WtoA(solved));
            row++;
        };
    }
    
    for (size_t idx = 0; idx < data.size(); ++idx)
        InsertRow(m_list, ids[idx], data[idx]);
}

void DbBrowserDialog::ResizeDialog(int minWidth)
{
    const int height = 400;
    MoveWindow(0, 0, 100, height);
    RECT clientRect, wndRect;
    GetClientRect(&clientRect);
    GetWindowRect(&wndRect);
    int wndClientDiffX = wndRect.right - clientRect.right;

    MoveWindow(0, 0, 1, height);  
    int gridWidth = m_list.ResizeColumns() + wndClientDiffX;
    MoveWindow(0, 0, gridWidth > minWidth ? gridWidth : minWidth, height);
    
    // Fill with grid
    GetClientRect(&clientRect);
    m_list.MoveWindow(0, 30, clientRect.right, clientRect.bottom);
}

} // namespace cuc