#include "pch.hpp"
#include "api/module.hpp"     // TODO: Should split off most loading/validation 
#include "db_browser.hpp"
#include "common/files.hpp"
#include "module_browser.hpp"
#include "module_system/module_manager.hpp"


namespace cuc
{

ModuleManager moduleManager;

struct ChildFrame : public CMDIChildWnd
{
    //DECLARE_DYNAMIC(ChildFrame)
    static CRuntimeClass classChildFrame; 
	virtual CRuntimeClass* GetRuntimeClass() const; 
    static CObject* PASCAL CreateObject();
    DECLARE_MESSAGE_MAP()
};

ModuleBrowser::ModuleBrowser()
    : ModelessDialog(L"Module Manager 3.0")
{
    AFX_WM_DRAW2D;
    //auto temp = new CMultiDocTemplate(0, RUNTIME_CLASS(ChildFrame), &ChildFrame::classChildFrame, RUNTIME_CLASS(ChildFrame));

    m_initLayoutProc = [this]() {
        MoveWindow(0, 0, 895, 400);

        ListBoxControl::Style listboxStyle = ListBoxControl::Style::Notify
                                           | ListBoxControl::Style::NoIntegralHeight;

        m_lists[Modules].Create(listboxStyle, GuiControl::Style::VScroll, { 20, 50, 200, 230 }, this);
        m_lists[Modules].SetHandler([&buttons = m_buttons, &groups = m_groups, &lists = m_lists]() {
            // TODO: deal with selecting multiple modules (deselect db, disable buttons, etc.)
            buttons[UpdateMod].Enable();
            buttons[RemoveMod].Enable();

            buttons[CreateDb].Enable();
            buttons[OpenDb].Enable();

            buttons[ViewDb].Disable();
            buttons[CloseDb].Disable();
            buttons[DeleteDb].Disable();

            groups[Databases].SetWindowTextW((L"Databases: " + lists[Modules].GetSelectedText()).c_str());

            lists[Databases].Clear();
            auto dbs = moduleManager.GetDbDescs(lists[Modules].GetSelectedIndex());
            for (auto& db : dbs)
                lists[Databases].AddSingleItem(db.GetName());
        });

        m_lists[Databases].Create(listboxStyle, GuiControl::Style::VScroll, { 460, 50, 640, 230 }, this);
        m_lists[Databases].SetHandler([&buttons = m_buttons]() {
            buttons[ViewDb].Enable();
            buttons[CloseDb].Enable();
            buttons[DeleteDb].Enable();
        });

        EditControl::Style metadataPanelStyle = EditControl::Style::Multiline
                                              | EditControl::Style::ReadOnly;
        m_metadataPanels[Modules].Create(metadataPanelStyle, GuiControl::Style::Default, { 220, 50, 420, 320 }, this);
        m_metadataPanels[Databases].Create(metadataPanelStyle, GuiControl::Style::Default, { 660, 50, 860, 320 }, this);

        // Buttons
        // Wizard y: 150..150+14
        // Here: height = 20;                        sep_y = 5
        //       width_wide = 180, width_short = 85; sep_x = 10

        m_buttons[AddMod].Create(L"Add...", { 20, 250, 200, 270 }, this);
        m_buttons[AddMod].SetHandler([&list = m_lists[Modules]]() {
            CFileDialog fileBrowser(true);
            auto& ofn = fileBrowser.GetOFN();

            ofn.lpstrDefExt = L".dll";
            ofn.Flags |= OFN_FILEMUSTEXIST | OFN_HIDEREADONLY | OFN_ALLOWMULTISELECT;
            ofn.lpstrFilter = L"Module (*.dll)\0*.dll\0";
            
            const size_t maxFilesNum = 50;
            const size_t bufferSz = maxFilesNum * (_MAX_PATH + 1) + 1;
            auto fileListBuffer = std::make_unique<wchar_t[]>(bufferSz);
            ofn.nMaxFile = bufferSz;
            ofn.lpstrFile = fileListBuffer.get();

            if (fileBrowser.DoModal() == IDOK)
            {
                auto pos = fileBrowser.GetStartPosition();
                while (pos)
                {
                    std::wstring path = (const wchar_t*)fileBrowser.GetNextPathName(pos);
                    try
                    { 
                        moduleManager.Load(path);
                    }
                    catch (const std::runtime_error& err)
                    {
                        ErrorMessageBox(err.what());
                        continue;
                    }
                    const int idx = list.GetCount();
                    list.AddSingleItem(moduleManager.GetModule(idx).GetName());
                }
            }
        });

        m_buttons[UpdateMod].Create(L"Update", { 20, 275, 200, 295 }, this);
        m_buttons[UpdateMod].Disable();
        m_buttons[UpdateMod].SetHandler([&list = m_lists[Modules]]() {
            int index = list.GetSelectedIndex();
            if (index < 0)
                return;
            // TODO: reload DB associations
            try
            {
                moduleManager.UpdateByName(list.GetSelectedText());
            }
            catch (std::runtime_error& err)
            {
                ErrorMessageBox(err.what());
                list.DeleteItemByIndex(index);
            }
        });

        m_buttons[RemoveMod].Create(L"Remove", { 20, 300, 200, 320 }, this);
        m_buttons[RemoveMod].Disable();
        m_buttons[RemoveMod].SetHandler([&list = m_lists[Modules]]() {
            moduleManager.Unload(list.GetSelectedText());
            list.DeleteSelected();
        });

        m_buttons[ViewDb].Create(L"View", { 460, 250, 640, 270 }, this);
        m_buttons[ViewDb].Disable();
        m_buttons[ViewDb].SetHandler([&moduleList = m_lists[Modules], &dbList = m_lists[Databases]]() {
            auto& dbs = moduleManager.GetDbDescs(moduleList.GetSelectedIndex());
            auto selName = dbList.GetSelectedText();
            std::wstring path;
            auto db = dbs.begin();
            for (; db != dbs.end(); ++db)
                if (db->GetName() == selName)
                    break;

            auto dlg = new DbBrowserDialog(*db, moduleManager);
            dlg->SetCaption(L"Browse database: ");
            dlg->Launch();
        });

        m_buttons[OpenDb].Create(L"Open", { 460, 275, 545, 295 }, this);
        m_buttons[OpenDb].Disable();
        m_buttons[OpenDb].SetHandler([&moduleList = m_lists[Modules], &dbList = m_lists[Databases]]() {
            CFileDialog fileBrowser(true);
            auto& ofn = fileBrowser.GetOFN();
            ofn.lpstrDefExt = L".cuc";
            ofn.lpstrFilter = L"matsel database (*.cuc)\0*.cuc\0";
            if (fileBrowser.DoModal() != IDOK)
                return;
            auto path = std::wstring(fileBrowser.GetPathName());

            auto desc = moduleManager.LoadDb(moduleList.GetSelectedIndex(), path, true);
            if (desc)
                dbList.AddSingleItem(desc->GetName());
        });

        m_buttons[CloseDb].Create(L"Close", { 555, 275, 640, 295 }, this);
        m_buttons[CloseDb].Disable();
        m_buttons[CloseDb].SetHandler([&moduleList = m_lists[Modules], &dbList = m_lists[Databases], &buttons = m_buttons]() {
            auto& dbs = moduleManager.GetDbDescs(moduleList.GetSelectedIndex());
            auto selName = dbList.GetSelectedText();
            std::wstring path;
            for (auto db = dbs.begin(); db != dbs.end(); ++db)
                if (db->GetName() == selName)
                {
                    path = db->GetPath();
                    dbs.erase(db);
                    break;
                }
            dbList.DeleteSelected();

            buttons[ViewDb].Disable();
            buttons[CloseDb].Disable();
            buttons[DeleteDb].Disable();
        });

        m_buttons[CreateDb].Create(L"Create", { 460, 300, 545, 320 }, this);
        m_buttons[CreateDb].Disable();
        m_buttons[CreateDb].SetHandler([&moduleList = m_lists[Modules], &dbList = m_lists[Databases]]() {
            CFileDialog fileBrowser(false);
            auto& ofn = fileBrowser.GetOFN();
            ofn.lpstrDefExt = L".cuc";
            ofn.lpstrFilter = L"matsel database (*.cuc)\0*.cuc\0";

            if (fileBrowser.DoModal() != IDOK)
                return;
            auto path = std::wstring(fileBrowser.GetPathName());

            auto desc = moduleManager.LoadDb(moduleList.GetSelectedIndex(), path, false);
            dbList.AddSingleItem(desc->GetName());
        });

        m_buttons[DeleteDb].Create(L"Delete", { 555, 300, 640, 320 }, this);
        m_buttons[DeleteDb].Disable();
        m_buttons[DeleteDb].SetHandler([&moduleList = m_lists[Modules], &dbList = m_lists[Databases], &buttons = m_buttons]() {
            auto& dbs = moduleManager.GetDbDescs(moduleList.GetSelectedIndex());
            auto selName = dbList.GetSelectedText();
            std::wstring path;
            for (auto db = dbs.begin(); db != dbs.end(); ++db)
                if (db->GetName() == selName)
                {
                    path = db->GetPath();
                    dbs.erase(db);
                    break;
                }
            _wremove(path.c_str()); // TODO: broken
            dbList.DeleteSelected();

            buttons[ViewDb].Disable();
            buttons[CloseDb].Disable();
            buttons[DeleteDb].Disable();
        });

        m_groups[Modules].Create(L"Modules", { 10, 20, 430, 330 }, this);
        m_groups[Databases].Create(L"Databases", { 450, 20, 870, 330 }, this);
    };

    m_initDataProc = [&modList = m_lists[Modules]]() {
        modList.AddItems(moduleManager.Deserialize());
    };
}

} // namespace cuc