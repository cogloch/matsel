#pragma once
#include "gui/dialog.hpp"


namespace cuc
{

// TODO: Force a single dummy per app instance 
class DummyDialog : public ModelessDialog
{
public:
    BOOL OnInitDialog() override;
};

} // namespace cuc