#pragma once


namespace cuc
{ 

using u8 = std::uint8_t;
using u16 = std::uint16_t;
using u32 = std::uint32_t;
using u64 = std::uint64_t;

using i8 = std::int8_t;
using i16 = std::int16_t;
using i32 = std::int32_t;
using i64 = std::int64_t;

using f32 = float;
using f64 = double;

// TODO: physical units 

// See: http://b.atch.se/posts/constexpr-counter/
template<int N>
struct flag {
    friend constexpr int adl_flag(flag<N>);
};

template<int N>
struct writer {
    friend constexpr int adl_flag(flag<N>) {
        return N;
    }

    static constexpr int value = N;
};

template<int N, int = adl_flag(flag<N> {}) >
int constexpr reader(int, flag<N>) {
    return N;
}

template<int N>
int constexpr reader(float, flag<N>, int R = reader(0, flag<N - 1> {})) {
    return R;
}

int constexpr reader(float, flag<0>) {
    return 0;
}

template<int N = 1>
int constexpr GetNextConst(int R = writer < reader(0, flag<32> {}) + N > ::value) {
    return R;
}

} // namespace cuc