#pragma once


namespace cuc
{

// TODO: temp; same as in module_manager.cpp
inline std::wstring GetDataPath()
{
    std::wstring exePath(MAX_PATH, '\0');
    GetModuleFileName(nullptr, exePath.data(), MAX_PATH);
    exePath = std::filesystem::path(exePath).parent_path().generic_wstring();
    const auto dataDir = exePath + L"/data/";
    if (!std::filesystem::exists(dataDir))
        std::filesystem::create_directory(dataDir);
    return dataDir;
}

} // namespace cuc