#pragma once
#include "mfcpp.hpp"
#include "common/common_types.hpp"


namespace cuc
{

class GuiControl
{
public:
    enum class Style : DWORD
    {
        Default  = 0,
        Child    = WS_CHILD,
        Visible  = WS_VISIBLE,
        TabStop  = WS_TABSTOP,
        Border   = WS_BORDER,
        VScroll  = WS_VSCROLL,
        Popup    = WS_POPUP,
        Caption  = WS_CAPTION,
        SysMenu  = WS_SYSMENU
    };

public:
    GuiControl();
    virtual ~GuiControl() = default;

    u32 GetId()
    {
        return m_resourceId;
    }

protected:
    /*static*/ const Style m_baseStyle;
    static u32 GetNextId()
    {
        return s_freeId++;
    }

    u32 m_resourceId = 0;

private:
    static u32 s_freeId; // TODO: Massive garbage but works for now
};

inline GuiControl::Style operator | (GuiControl::Style lhs, GuiControl::Style rhs)
{
    return static_cast<GuiControl::Style>(static_cast<DWORD>(lhs) | static_cast<DWORD>(rhs));
}

inline GuiControl::Style& operator |= (GuiControl::Style & lhs, GuiControl::Style rhs)
{
    lhs = lhs | rhs;
    return lhs;
}

} // namespace cuc