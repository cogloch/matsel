#pragma once
#include "gui_control.hpp"


namespace cuc
{

class Dialog : public CDialog, public GuiControl
{
    DECLARE_MESSAGE_MAP()

public:
    enum class Style : DWORD
    {
        SetFont         = DS_SETFONT,
        FixedSys        = DS_FIXEDSYS,
        ModalFrame      = DS_MODALFRAME,
        SetForeground   = DS_SETFOREGROUND
    };

public:
    virtual ~Dialog() = default;

    // Override 
    // BOOL OnInitDialog() 
    //     CDialog::OnInitDialog()
    //     <init layout, data, etc.> 
    // Called after ::Launch()

    // Useful before ::Launch(); alternative to ctor caption
    void SetCaption(const std::wstring&);

    BOOL OnInitDialog() override;

protected:
    Dialog(const std::wstring& caption = L"");

    std::wstring m_caption;
    DLGTEMPLATE m_template;
    class TemplateBlock
    {
    public:
        TemplateBlock(DLGTEMPLATE& mainTemplate, const std::wstring& caption, const std::wstring& fontface = L"MS Shell Dlg");
        ~TemplateBlock();
        const DLGTEMPLATE* const Get() const;

    private:
        u8*    m_buffer;
        HLOCAL m_memBlock;
    };

    void OnClose();

    // Circumvent the whole MFC message map bullshit (with a little overhead)
    // Assign handlers in ctors of derived classes 
    std::function<void()> m_initLayoutProc;
    std::function<void()> m_initDataProc;

private:
    static int s_numWindows; // Track real (not dummy) windows; kill app when reaching 0 on OnClose()
};

// Blocks on ::Launch(); returns user action to callee 
class ModalDialog : public Dialog
{
public:
    ModalDialog(const std::wstring& caption = L"");
    virtual ~ModalDialog() = default;
    int Launch();
};

// Non-blocking on ::Launch(); no result to callee 
class ModelessDialog : public Dialog
{
public:
    ModelessDialog(const std::wstring& caption = L"");
    virtual ~ModelessDialog() = default;
    void Launch();
};

// Flags 
inline Dialog::Style operator | (Dialog::Style lhs, Dialog::Style rhs)
{
    return static_cast<Dialog::Style>(static_cast<DWORD>(lhs) | static_cast<DWORD>(rhs));
}

inline Dialog::Style& operator |= (Dialog::Style & lhs, Dialog::Style rhs)
{
    lhs = lhs | rhs;
    return lhs;
}

inline DWORD operator | (Dialog::Style lhs, GuiControl::Style rhs)
{
    return (static_cast<DWORD>(lhs) | static_cast<DWORD>(rhs));
}

inline DWORD operator | (GuiControl::Style lhs, Dialog::Style rhs)
{
    return (static_cast<DWORD>(lhs) | static_cast<DWORD>(rhs));
}

} // namespace cuc