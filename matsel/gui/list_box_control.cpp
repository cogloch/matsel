#include "pch.hpp"
#include "list_box_control.hpp"


namespace cuc
{

BEGIN_MESSAGE_MAP(ListBoxControl, CListBox)
    ON_WM_LBUTTONUP() // Rely on this instead of SELCHANGE to avoid resource IDs
END_MESSAGE_MAP()

BOOL ListBoxControl::Create(ListBoxControl::Style controlStyle, GuiControl::Style windowStyle, const RECT& rect, CWnd* parent)
{
    u32 id = GetNextId();
    SubclassDlgItem(id, parent);
    windowStyle |= m_baseStyle;
    auto ret = CListBox::Create(controlStyle | windowStyle, rect, parent, id);
    SetFont(parent->GetFont());
    return ret;
}

void ListBoxControl::Clear()
{
    while (GetCount())
        DeleteString(0);
    m_lastItem = -1;
}

void ListBoxControl::AddSingleItem(const std::wstring& text)
{
    AddString(text.c_str());
}

void ListBoxControl::AddItems(const std::vector<std::wstring>& list)
{
    for (const auto& item : list)
        AddString(item.c_str()); 
}

void ListBoxControl::ReplaceAllItems(const std::vector<std::wstring>& list)
{
    Clear();
    AddItems(list);
}

int ListBoxControl::GetSelectedIndex()
{
    return GetCurSel();
}

std::wstring ListBoxControl::GetSelectedText()
{
    return GetItemText(GetSelectedIndex());
}

void ListBoxControl::DeleteSelected()
{
    DeleteItemByIndex(GetSelectedIndex());
    // TODO: update m_lastItem
}

void ListBoxControl::DeleteItemByIndex(u32 index)
{
    DeleteString(index);
    // TODO: update m_lastItem
}

std::wstring ListBoxControl::GetItemText(int index)
{
    if (GetCount() < index) return L"";
    const auto len = GetTextLen(index);
    std::wstring text(len, '\0');
    GetText(index, text.data());
    return text;
}

void ListBoxControl::OnLButtonUp(UINT flags, CPoint point)
{
    int sel = GetCurSel();
    if (sel == -1 || sel == m_lastItem)
        return;
    m_lastItem = sel;

    if (m_handler)
        m_handler();
    CListBox::OnLButtonUp(flags, point);
}

void ListBoxControl::SetHandler(OnSelectItemFunc handler)
{
    m_handler = std::move(handler);
}

ListBoxControl::Style operator | (ListBoxControl::Style lhs, ListBoxControl::Style rhs)
{
    return static_cast<ListBoxControl::Style>(static_cast<DWORD>(lhs) | static_cast<DWORD>(rhs));
}

ListBoxControl::Style& operator |= (ListBoxControl::Style & lhs, ListBoxControl::Style rhs)
{
    lhs = lhs | rhs;
    return lhs;
}

DWORD operator | (ListBoxControl::Style lhs, GuiControl::Style rhs)
{
    return (static_cast<DWORD>(lhs) | static_cast<DWORD>(rhs));
}

DWORD operator | (GuiControl::Style lhs, ListBoxControl::Style rhs)
{
    return (static_cast<DWORD>(lhs) | static_cast<DWORD>(rhs));
}

} // namespace cuc