#pragma once
#include "gui_control.hpp"


namespace cuc
{

class ListGridControl : public CListCtrl, public GuiControl
{
    DECLARE_MESSAGE_MAP();

public:
    void Create(const RECT& rect, CWnd* parent);
    int ResizeColumns();

    int GetRowKey(int row);
    std::vector<std::wstring> GetRowData(int row);
    
    void Deselect();

    void AddColumn(const std::string& headerA);
    void AddColumns(const std::vector<std::string>& headers);

    using OnDoubleClick    = std::function<void()>;
    using OnSelectItemFunc = std::function<void()>;

    void OnLButtonDblClk(UINT flags, CPoint pt);
    void OnLButtonDown(UINT flags, CPoint pt);

    void SetHandlerDblClick(OnDoubleClick handler);
    void SetHandlerSelect(OnSelectItemFunc handler);

    using OnKeyUpFunc = std::function<void(UINT)>;
    void SetHandlerKeyUp(OnKeyUpFunc); // TODO: GuiControl
    void OnKeyUp(UINT, UINT, UINT);

    int GetSelectedRow();
    int GetSelectedCol();

private:
    OnSelectItemFunc m_handlerSelect = nullptr;
    OnSelectItemFunc m_handlerDblClk = nullptr;
    OnKeyUpFunc      m_handlerKeyUp  = nullptr;

    int m_numColumns = 0; // TODO: use the header control count instead

    int m_selRow = -1;
    int m_selCol = -1;
};

} // namespace cuc