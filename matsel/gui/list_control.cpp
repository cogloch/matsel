#include "pch.hpp"
#include "list_control.hpp"


namespace cuc
{

BEGIN_MESSAGE_MAP(ListGridControl, CListCtrl)
    ON_WM_LBUTTONDOWN()
    ON_WM_LBUTTONDBLCLK()
    ON_WM_KEYUP()
END_MESSAGE_MAP()

void ListGridControl::Create(const RECT& rect, CWnd* parent)
{
    u32 id = GetNextId();
    SubclassDlgItem(id, parent);
    CListCtrl::Create((DWORD)m_baseStyle | LVS_REPORT, rect, parent, id);
    SetExtendedStyle(LVS_EX_FULLROWSELECT | LVS_EX_GRIDLINES);
    SetFont(parent->GetFont());
}

void ListGridControl::OnLButtonDblClk(UINT flags, CPoint pt)
{
    // Sent after OnLButtonDown

    if (m_selRow == -1 || m_selCol == -1)
        return;

    if (m_handlerDblClk)
        m_handlerDblClk();

    CListCtrl::OnMButtonDblClk(flags, pt);
}

void ListGridControl::OnLButtonDown(UINT flags, CPoint pt)
{
    LVHITTESTINFO htinfo;
    htinfo.pt = pt;
    SubItemHitTest(&htinfo);

    if (htinfo.flags & LVHT_ONITEM)
    {
        m_selRow = htinfo.iItem;
        m_selCol = htinfo.iSubItem;
        
        if (m_handlerSelect)
            m_handlerSelect();
    }
    else
    {
        Deselect();
    }

    CListCtrl::OnLButtonUp(flags, pt);
}

int ListGridControl::GetRowKey(int row)
{
    return std::stoi(GetItemText(row, 0).GetString());
}

std::vector<std::wstring> ListGridControl::GetRowData(int row)
{
    std::vector<std::wstring> result;
    for (int col = 1; col < m_numColumns; ++col) // Ignore key col (col=0)
    {
        result.push_back(GetItemText(row, col).GetString());
    }
    return result;
}

void ListGridControl::Deselect()
{
    m_selRow = -1;
    m_selCol = -1;
}

int ListGridControl::ResizeColumns()
{
    int totalWidth = 0;
    SetRedraw(FALSE);
    // TODO: resize based on share of total dialog wnd width 
    for (int i = 0; i < m_numColumns; ++i)
    {
        SetColumnWidth(i, LVSCW_AUTOSIZE);
        auto dataWidth = GetColumnWidth(i);
        SetColumnWidth(i, LVSCW_AUTOSIZE_USEHEADER);
        auto headerWidth = GetColumnWidth(i);
        auto maxWidth = dataWidth > headerWidth ? dataWidth : headerWidth;
        maxWidth *= 2;
        SetColumnWidth(i, maxWidth);
        totalWidth += maxWidth;
    }
    SetRedraw(TRUE);
    return totalWidth;
}

void ListGridControl::AddColumn(const std::string& headerA)
{
    std::wstring header(((CStringW)(headerA.c_str())).GetString()); // wtf am i doing 
    InsertColumn(m_numColumns, header.c_str(), LVCFMT_CENTER);
    m_numColumns++;
}

void ListGridControl::AddColumns(const std::vector<std::string>& headers)
{
    for (const auto& header : headers)
        AddColumn(header);
}

void ListGridControl::SetHandlerDblClick(OnDoubleClick handler)
{
    m_handlerDblClk = std::move(handler);
}

void ListGridControl::SetHandlerSelect(OnSelectItemFunc handler)
{
    m_handlerSelect = std::move(handler);
}

void ListGridControl::SetHandlerKeyUp(OnKeyUpFunc handler)
{
    m_handlerKeyUp = handler;
}

void ListGridControl::OnKeyUp(UINT ch, UINT p2, UINT p3)
{
    if(m_handlerKeyUp)
        m_handlerKeyUp(ch);

    CListCtrl::OnKeyUp(ch, p2, p3);
}

int ListGridControl::GetSelectedRow() { return m_selRow; }
int ListGridControl::GetSelectedCol() { return m_selCol; }

} // namespace cuc 