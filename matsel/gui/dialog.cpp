#include "pch.hpp"
#include "dialog.hpp"


namespace cuc
{

BEGIN_MESSAGE_MAP(Dialog, CDialog)
    ON_WM_CLOSE()
END_MESSAGE_MAP()

int Dialog::s_numWindows = -1;

void Dialog::SetCaption(const std::wstring& caption)
{
    m_caption = caption;
}

BOOL Dialog::OnInitDialog()
{
    CDialog::OnInitDialog();

    m_initLayoutProc();
    m_initDataProc();

    return true;
}

Dialog::Dialog(const std::wstring& caption)
    : m_template{}
    , m_caption(caption)
{
    const auto dlgStyle = Style::FixedSys
                        | Style::SetFont
                        | Style::ModalFrame
                        | Style::SetForeground;
    const auto wndStyle = GuiControl::Style::Popup
                        | GuiControl::Style::Caption
                        | GuiControl::Style::SysMenu
                        | GuiControl::Style::Visible;

    m_template.style = dlgStyle | wndStyle;

    s_numWindows++;

    m_initLayoutProc = [](){};
    m_initDataProc   = [](){};
}

void Dialog::OnClose()
{
    CDialog::OnClose();
    s_numWindows--;
    if (s_numWindows < 1)
        PostQuitMessage(0);
}

ModalDialog::ModalDialog(const std::wstring& caption)
    : Dialog(caption)
{
}

int ModalDialog::Launch()
{
    TemplateBlock block(m_template, m_caption);
    InitModalIndirect(block.Get(), nullptr);
    return CDialog::DoModal();
}

ModelessDialog::ModelessDialog(const std::wstring& caption)
    : Dialog(caption)
{
}

void ModelessDialog::Launch()
{
    TemplateBlock block(m_template, m_caption);
    CreateIndirect(block.Get(), nullptr);
}

Dialog::TemplateBlock::TemplateBlock(DLGTEMPLATE& mainTemplate, const std::wstring& caption, const std::wstring& fontface)
{
    const size_t captionSz = sizeof(wchar_t) * (caption.size() + 1);     // Add \0
    const size_t fontfaceSz = sizeof(wchar_t) * (fontface.size() + 1);   // Add \0
    const int buffSz = sizeof(DLGTEMPLATE) 
                     + sizeof(DWORD) 
                     + captionSz
                     + sizeof(WORD) 
                     + fontfaceSz;

    m_memBlock = LocalAlloc(LHND, buffSz);
    m_buffer = static_cast<u8*>(LocalLock(m_memBlock));
    auto dest = m_buffer;

    // Template
    memcpy(dest, &mainTemplate, sizeof(DLGTEMPLATE));
    dest += sizeof(DLGTEMPLATE);

    // Menu, window class 
    *(WORD*)dest = 0;
    dest += sizeof(WORD);
    *(WORD*)(dest + 1) = 0;
    dest += sizeof(WORD);

    // Caption
    memcpy(dest, caption.c_str(), captionSz);
    dest += captionSz;

    // Font size
    *(WORD*)dest = 8;
    dest += sizeof(WORD);

    // Font face 
    memcpy(dest, fontface.c_str(), fontfaceSz);
    dest += fontfaceSz;
}

Dialog::TemplateBlock::~TemplateBlock()
{
    LocalUnlock(m_memBlock); 
    LocalFree(m_memBlock);
}

const DLGTEMPLATE* const Dialog::TemplateBlock::Get() const
{
    return reinterpret_cast<DLGTEMPLATE*>(m_buffer);
}

} // namespace cuc 