#pragma once
#include "gui_control.hpp"
#include "common/common_types.hpp"


namespace cuc
{

class ListBoxControl : public CListBox, GuiControl
{
    DECLARE_MESSAGE_MAP();

public:
    enum class Style : DWORD
    {
        Sort             = LBS_SORT,
        Notify           = LBS_NOTIFY,
        NoIntegralHeight = LBS_NOINTEGRALHEIGHT
    };

public:
    BOOL Create(Style, GuiControl::Style windowStyle, const RECT& rect, CWnd* parent);
    
    void Clear();
    void AddSingleItem(const std::wstring& text);
    void AddItems(const std::vector<std::wstring>& list);
    void ReplaceAllItems(const std::vector<std::wstring>& list);
    int GetSelectedIndex();
    std::wstring GetSelectedText();
    void DeleteSelected();
    void DeleteItemByIndex(u32 index);
    std::wstring GetItemText(int index);

    void OnLButtonUp(UINT flags, CPoint point);
    using OnSelectItemFunc = std::function<void()>;
    void SetHandler(OnSelectItemFunc handler);

private:
    OnSelectItemFunc m_handler = nullptr;
    int m_lastItem = -1;                     // TODO: this ignores items being removed (updating indices)
};

// Flags 
ListBoxControl::Style operator | (ListBoxControl::Style lhs, ListBoxControl::Style rhs);
ListBoxControl::Style& operator |= (ListBoxControl::Style & lhs, ListBoxControl::Style rhs);
DWORD operator | (ListBoxControl::Style lhs, GuiControl::Style rhs);
DWORD operator | (GuiControl::Style lhs, ListBoxControl::Style rhs);

} // namespace cuc 