#pragma once
#include "mfcpp.hpp"
#include "gui_control.hpp"
#include "common/common_types.hpp"


namespace cuc
{

class GroupBox : public CButton, GuiControl
{
public:
    void Create(const wchar_t* label, const RECT& rect, CWnd* parent, DWORD style = WS_CHILD | WS_VISIBLE)
    {
        auto id = GetNextId();
        SubclassDlgItem(id, parent);
        CButton::Create(label, style | BS_GROUPBOX, rect, parent, id);
        SetFont(parent->GetFont());
    }
};

class PushButton : public CButton, GuiControl
{
    DECLARE_MESSAGE_MAP();

public:
    void Create(const std::wstring& label, const RECT& rect, CWnd* parent, DWORD style = WS_CHILD | WS_VISIBLE)
    {
        auto id = GetNextId();
        SubclassDlgItem(id, parent);
        CButton::Create(label.c_str(), style | BS_PUSHBUTTON, rect, parent, id);
        SetFont(parent->GetFont());
    }

    void OnLButtonUp(UINT flags, CPoint point)
    {
        //__debugbreak();
        if (m_handler)
            m_handler();
        CButton::OnLButtonUp(flags, point);
    }

    void Enable()
    {
        EnableWindow(true);
    }

    void Disable()
    {
        EnableWindow(false);
    }

public:
    using OnClickFunc = std::function<void()>;
    
    void SetHandler(OnClickFunc handler)
    {
        m_handler = std::move(handler);
    }

private:
    OnClickFunc m_handler = nullptr;
};

} // namespace cuc