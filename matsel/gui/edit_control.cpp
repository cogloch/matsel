#include "pch.hpp"
#include "edit_control.hpp"


namespace cuc
{

BOOL EditControl::Create(EditControl::Style controlStyle, GuiControl::Style windowStyle, const RECT& rect, CWnd* parent)
{
    m_resourceId = GetNextId();
    SubclassDlgItem(m_resourceId, parent);
    windowStyle |= m_baseStyle;
    auto ret = CEdit::Create(controlStyle | windowStyle | ES_AUTOHSCROLL, rect, parent, m_resourceId);
    SetFont(parent->GetFont());
    return ret;
}

BOOL EditControl::PreTranslateMessage(MSG* msg)
{
	if (msg->message == WM_KEYUP)
	{
		if (msg->wParam == 'A' && GetKeyState(VK_CONTROL) < 0)
		{
			if (auto edit = dynamic_cast<CEdit*>(GetFocus()))
			{
				edit->SetSel(0, -1, FALSE);
				return TRUE;
			}
		}
	}

	return CEdit::PreTranslateMessage(msg);
}

} // namespace cuc