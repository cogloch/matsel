#include "pch.hpp"
#include "button_control.hpp"


namespace cuc
{

BEGIN_MESSAGE_MAP(PushButton, CButton)
    ON_WM_LBUTTONUP()
END_MESSAGE_MAP()

} // namespace cuc