#pragma once
#include "gui_control.hpp"


namespace cuc
{

class EditControl : public CEdit, public GuiControl
{
public:
    enum class Style : DWORD
    {
        Left      = ES_LEFT,
        Multiline = ES_MULTILINE,
        ReadOnly  = ES_READONLY
    };

public:
    BOOL Create(EditControl::Style, GuiControl::Style windowStyle, const RECT& rect, CWnd* parent);

	BOOL PreTranslateMessage(MSG*) override;
};

// Flags 
inline EditControl::Style operator | (EditControl::Style lhs, EditControl::Style rhs)
{
    return static_cast<EditControl::Style>(static_cast<DWORD>(lhs) | static_cast<DWORD>(rhs));
}

inline EditControl::Style& operator |= (EditControl::Style& lhs, EditControl::Style rhs)
{
    lhs = lhs | rhs;
    return lhs;
}

inline DWORD operator | (EditControl::Style lhs, GuiControl::Style rhs)
{
    return (static_cast<DWORD>(lhs) | static_cast<DWORD>(rhs));
}

inline DWORD operator | (GuiControl::Style lhs, EditControl::Style rhs)
{
    return (static_cast<DWORD>(lhs) | static_cast<DWORD>(rhs));
}

} // namespace cuc