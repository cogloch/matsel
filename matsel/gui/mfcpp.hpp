#pragma once


////////
// Very simple wrappers over winapi/mfc stuff to make them less annoying (+other utilities)
////////

namespace cuc
{

#undef LoadLibrary
inline HMODULE LoadLibrary(const std::wstring& filename)
{
    return LoadLibraryW(filename.c_str());
}

#undef GetModuleHandle
inline HMODULE GetModuleHandle(const std::wstring& moduleName)
{
    return GetModuleHandleW(moduleName.c_str());
}

using ProcPtr = int(__stdcall*)();
inline ProcPtr GetProcAddress(HMODULE moduleHandle, const std::string& procName)
{
    return ::GetProcAddress(moduleHandle, procName.c_str());
}

inline std::vector<std::string> EnumDLLSymbols(const std::string& dllName)
{
    std::vector<std::string> symbols;

    _LOADED_IMAGE loadedImg;
    if (!MapAndLoad(dllName.c_str(), nullptr, &loadedImg, true, true))
        return symbols;

    unsigned long dirSize = 0;
    auto exportDir = static_cast<_IMAGE_EXPORT_DIRECTORY*>(ImageDirectoryEntryToData(loadedImg.MappedAddress, false, IMAGE_DIRECTORY_ENTRY_EXPORT, &dirSize));
    if (!exportDir)
    {
        UnMapAndLoad(&loadedImg);
        return symbols;
    }

    auto nameRVAs = static_cast<DWORD*>(ImageRvaToVa(loadedImg.FileHeader, loadedImg.MappedAddress, exportDir->AddressOfNames, nullptr));
    for (size_t i = 0; i < exportDir->NumberOfNames; ++i)
        symbols.push_back(static_cast<char*>(ImageRvaToVa(loadedImg.FileHeader, loadedImg.MappedAddress, nameRVAs[i], nullptr)));

    UnMapAndLoad(&loadedImg);

    return symbols;
}

// Filename without extension *.
inline std::wstring FilenameFromPathNoExt(const std::wstring& path, const std::wstring& ext)
{
    std::wstring filename = path;
    filename = std::filesystem::path(filename).filename();
    filename = filename.erase(filename.find(ext));
    return filename;
}

inline std::string WtoA(const std::wstring& str)
{
    return std::string((CStringA)str.c_str());
}

inline std::wstring AtoW(const std::string& str)
{
    return std::wstring(((CStringW)(str.c_str())).GetString());
}

inline void ErrorMessageBox(const std::wstring& msg)
{
    MessageBoxW(nullptr, msg.c_str(), L"Error", MB_ICONERROR);
}

inline void ErrorMessageBox(const std::string& msg)
{
    MessageBoxA(nullptr, msg.c_str(), "Error", MB_ICONERROR);
}

} // namespace cuc
