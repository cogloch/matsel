#include "pch.hpp"
#include "db_instance.hpp"
#include "gui/mfcpp.hpp"
#include "common/common_types.hpp"


namespace cuc
{

DbInstance::DbInstance(const CString& path, CoreSchema& schema) 
    : DbInstance(std::wstring(path), schema)
{
}

DbInstance::DbInstance(const std::wstring& path, CoreSchema& schema)
    : m_coreSchema(schema)
{
    m_path = path;
    m_name = FilenameFromPathNoExt(path, L".cuc");

    // TODO: force ctor w/ sqlite db ptr 
    // TODO: handle move/copy ctors 
    // TODO: generate prepared statements (common to all instances for one module)
}

DbInstance::~DbInstance()
{
    /*
    if (m_psDeleteRow)       delete m_psDeleteRow;
    if (m_psCountRows)       delete m_psCountRows;
    if (m_psUpdateRow)       delete m_psUpdateRow;
    if (m_psNullifyRow)      delete m_psNullifyRow;
    //if (m_psGetColByKey)     delete m_psGetColByKey;
    if (m_psInsertFullRow)   delete m_psInsertFullRow;
    if (m_psGetNonNullKeys)  delete m_psGetNonNullKeys;
    */
}

const std::wstring& DbInstance::GetName() const
{
    return m_name;
}

void DbInstance::ConnectSql()
{
    //db = std::make_shared<sqlite::database>(GetPathA());
    db = new sqlite::database(GetPathA());

    // General cache
    //if (!m_psGetColByKey)
    //{
    //    // Cache
    //    auto ps = (*db) << "select ? from mat where _id = ?;";
    //    m_psGetColByKey = new sqlite::database_binder(std::move(ps));
    //}
}

void DbInstance::CreateMainTable()
{
    // TODO: key column to include (hidden) module name and be used in validating association with said module 
    // All data as ::String, decode based  on identifier in (hidden) header, e.g. density: real / "3.1456", not 3.1456
    std::string query = "create table if not exists " + m_mainTable + " (_id integer primary key autoincrement not null";

    for (auto& col : m_coreSchema)
        query += ", " + col.first + " text";

    query += ");";
    *db << query;
}

void DbInstance::UpdateRow(i64 key, std::vector<std::string>& vals)
{
    if (!m_psUpdateRow)
    {
        // Cache
        std::string query = "update " + m_mainTable + " set ";
        for (auto& col : m_coreSchema)
            query += col.first + " = ?, ";
        query.pop_back(); query.pop_back();
        query += " where _id = ?;";
        auto ps = (*db) << query;
        m_psUpdateRow = new sqlite::database_binder(std::move(ps));
    }

    for (auto& val : vals)
        *m_psUpdateRow << val;
    *m_psUpdateRow << key;
    m_psUpdateRow->execute();
}

void DbInstance::InsertRow(std::vector<std::string> & vals)
{
    if (vals.size() != m_coreSchema.size())
        vals.resize(m_coreSchema.size());

    if (!m_psInsertFullRow)
    {
        // Cache ps 
        std::string query = "insert into " + m_mainTable + " (";

        // Schema specific to module  
        // Validate based on existence of required columns alone
        for (auto& col : m_coreSchema)
            query += col.first + ",";

        query.pop_back();
        std::string tokens;
        for (int i = 0, numTokens = m_coreSchema.size(); i < numTokens; ++i)
            tokens += "?,";
        tokens.pop_back();
        query += ") values (" + tokens + ");";

        auto ps = (*db) << query;
        m_psInsertFullRow = new sqlite::database_binder(std::move(ps));
    }

    for (auto& val : vals)
        *m_psInsertFullRow << val;
    m_psInsertFullRow->execute();
}

void DbInstance::DeleteRow(i64 key)
{
    if (!m_psDeleteRow)
    {
        // Cache
        std::string query = "delete from " + m_mainTable + " where _id = ?;";
        auto ps = (*db) << query;
        m_psDeleteRow = new sqlite::database_binder(std::move(ps));
    }

    *m_psDeleteRow << key;
    m_psDeleteRow->execute();
}

void DbInstance::BeginTransaction()
{
    *db << "begin;";
}

void DbInstance::EndTransaction()
{
    *db << "commit;";
}

void DbInstance::NullifyRow(i64 key)
{
    if (!m_psNullifyRow)
    {
        // Cache
        std::string query = "update " + m_mainTable + " set ";
        for (auto& col : m_coreSchema)
            query += col.first + " = null, ";
        query.pop_back(); query.pop_back();
        query += " where _id = ?;";
        auto ps = (*db) << query;
        m_psNullifyRow = new sqlite::database_binder(std::move(ps));
    }

    *m_psNullifyRow << key;
    m_psNullifyRow->execute();
}

int DbInstance::CountRows()
{
    if (!m_psCountRows)
    {
        // Cache
        auto ps = (*db) << "select count(*) from " + m_mainTable;
        m_psNullifyRow = new sqlite::database_binder(std::move(ps));
    }

    int count = 0;
    *m_psDeleteRow >> count;
    return count;
}

std::vector<int> DbInstance::GetNonNullKeys()
{
    if (!m_psGetNonNullKeys)
    {
        // Cache
        auto ps = (*db) << "select _id from " + m_mainTable + ";";
        m_psGetNonNullKeys = new sqlite::database_binder(std::move(ps));
    }

    std::vector<int> ids;
    *m_psGetNonNullKeys >> [&](int id) {
        ids.push_back(id);
    };
    //m_psGetNonNullKeys->execute();
    return ids;
}

std::wstring DbInstance::GetValueByKey(i64 key, std::wstring col) const
{
    // Cache done on connection 

    //std::string ret;
    //*db << "select source$text from mat where _id = ?;" << 35 >> ret; // ok
    //*db << "select ? from mat where _id = ?;" << "source$text" << 35 >> ret;  // not ok

    std::string colA = WtoA(col);
    std::string result;
    *db << "select " + colA + " from " + m_mainTable + " where _id = ?;" << key >> [&](std::unique_ptr<std::string> ret) {
        result = ret ? *ret : "";
    };
    //*m_psGetColByKey << colA << key;
    //*m_psGetColByKey >> result;
    return AtoW(result);
}

} // namespace cuc 