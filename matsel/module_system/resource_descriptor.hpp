#pragma once


namespace cuc
{

class ResourceDescriptor
{
public:
    virtual ~ResourceDescriptor() = default;
    const std::wstring& GetPath() const;
    std::string GetPathA() const;

protected:
    std::wstring m_path;
};

} // namespace cuc 