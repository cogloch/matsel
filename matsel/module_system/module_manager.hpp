#pragma once
#include "module_descriptor.hpp"


namespace cuc
{

// For now, identify modules by name only, i.e. require unique names 
class ModuleManager
{
public:
    void Load(const std::wstring& path);
    void UpdateByName(const std::wstring& name);
    void Unload(const std::wstring& path);
    bool IsLoaded(const std::wstring& path);

    // Assume same filename, in the working directory, at all times 
    void Serialize();
    std::vector<std::wstring> Deserialize();

    void AddDbDesc(size_t moduleIdx, DbInstance);
    std::list<DbInstance>& GetDbDescs(size_t moduleIdx);

    ModuleDescriptor& GetModule(size_t idx);

    // DB
    // Fail if file doesn't exist
    // TODO: Check if file is actually valid
    const DbInstance* LoadDb(size_t moduleIdx, const std::wstring& path, bool mustExist = true);

    void SolveReference(DataReference& ref) const;

    const ModuleDescriptor* GetModulePtrByName(const std::wstring& name) const;

private:
    std::optional<size_t> GetIndexByName(const std::wstring& name);
    std::list<ModuleDescriptor> m_loadedModules;
};

} // namespace cuc 