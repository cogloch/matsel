#include "pch.hpp"
#include "module_descriptor.hpp"


namespace cuc 
{

const std::wstring& ModuleDescriptor::GetName() const
{
    return m_name;
}

bool ModuleDescriptor::Load()
{
    m_winHandle = LoadLibrary(m_path);
    return m_winHandle;
}

std::vector<size_t> ModuleDescriptor::LoadSymbols(const std::vector<std::string>& required)
{
    // TODO: allow missing symbols if they are not critical; still log, display in browser, etc. 
    std::vector<ProcPtr> funcs(required.size());
    std::vector<size_t> missing;

    for (size_t symbolIdx = 0, numSymbols = required.size(); symbolIdx < numSymbols; ++symbolIdx)
    {
        funcs[symbolIdx] = GetProcAddress(m_winHandle, required[symbolIdx]);
        if (!funcs[symbolIdx]) missing.push_back(symbolIdx);
    }

    if (missing.empty())
    {
        m_createModuleProc = reinterpret_cast<void(*)(CreateModuleArgs*)>(funcs[0]);
        m_destroyModuleProc = reinterpret_cast<void(*)()>(funcs[1]);
        m_submitDataProc = reinterpret_cast<void(*)(void*, size_t)>(funcs[2]);
    }

    return missing;
}

std::vector<std::string> ModuleDescriptor::GetExtraSymbols(const std::vector<std::string>& required)
{
    std::vector<std::string> extra;
    const auto nameA = CStringA(m_name.c_str()); //path
    auto allSymbols = EnumDLLSymbols((const char*)nameA);
    for (size_t existingIdx = 0, numExisting = allSymbols.size(); existingIdx < numExisting; ++existingIdx)
    {
        bool found = false;
        for (const auto& requiredS : required)
            if (allSymbols[existingIdx] == requiredS)
            {
                found = true;
                break;
            }

        if (!found)
        {
            extra.push_back(allSymbols[existingIdx]);
        }
    }

    return extra;
}

ModuleDescriptor::ModuleDescriptor()
    : m_winHandle(nullptr)
{
}

ModuleDescriptor::ModuleDescriptor(const std::wstring& path)
    : m_winHandle(nullptr)
{
    m_path = path;
    m_name = FilenameFromPathNoExt(path, L".dll");
    auto time = std::time(nullptr);
    m_updateTimestamp = _wasctime(std::localtime(&time));
}

ModuleDescriptor::~ModuleDescriptor()
{
    //if (m_winHandle)
    //    FreeLibrary(m_winHandle);
}

//ModuleDescriptor::ModuleDescriptor(ModuleDescriptor&& other)
//{
//    m_name = other.m_name;
//    m_path = other.m_path;
//    m_updateTimestamp = other.m_updateTimestamp;
//
//    m_winHandle = other.m_winHandle;
//    other.m_winHandle = nullptr;
//    
//    m_createModuleProc = other.m_createModuleProc;
//    other.m_createModuleProc = nullptr;
//    
//    m_destroyModuleProc = other.m_destroyModuleProc;
//    other.m_destroyModuleProc = nullptr;
//    
//    m_submitDataProc = other.m_submitDataProc;
//    other.m_submitDataProc = nullptr;
//
//    m_dbDescs = other.m_dbDescs;
//    m_coreSchema = other.m_coreSchema;
//}
//
//ModuleDescriptor& ModuleDescriptor::operator=(ModuleDescriptor&& other)
//{
//    m_name = other.m_name;
//    m_path = other.m_path;
//    m_updateTimestamp = other.m_updateTimestamp;
//
//    m_winHandle = other.m_winHandle;
//    other.m_winHandle = nullptr;
//    
//    m_createModuleProc = other.m_createModuleProc;
//    other.m_createModuleProc = nullptr;
//    
//    m_destroyModuleProc = other.m_destroyModuleProc;
//    other.m_destroyModuleProc = nullptr;
//
//    m_submitDataProc = other.m_submitDataProc;
//    other.m_submitDataProc = nullptr;
//
//    m_dbDescs = other.m_dbDescs;
//    m_coreSchema = other.m_coreSchema;
//
//    return *this;
//}

HMODULE ModuleDescriptor::GetHandle()
{
    return m_winHandle;
}

void ModuleDescriptor::InitModuleMetadata(/*std::string name, */std::string description, Schema* schema)
{
    // TODO: validate args (not specific to module)
    m_description = (CStringW)description.c_str();

    if (schema)
    {
        // std::vector<std::pair<std::string, SchemaType>> m_coreSchema;
        for (size_t idx = 0; idx < schema->numLabels; ++idx)
        {
            m_coreSchema.push_back(std::make_pair(schema->labels[idx], schema->types[idx]));
        }
    }
}

const DbInstance* ModuleDescriptor::GetDbInstanceByName(const std::wstring& name) const
{
    for (auto it = m_dbInstances.begin(); it != m_dbInstances.end(); ++it)
        if (it->GetName() == name)
            return &*it;
    return nullptr;
}

void ModuleDescriptor::CreateModule(CreateModuleArgs* args)
{
    if (m_createModuleProc)
        m_createModuleProc(args);
    else 
        throw L"Module " + m_name + L":: No CreateModule symbol.";
}

void ModuleDescriptor::DestroyModule()
{
    if (m_destroyModuleProc)
        m_destroyModuleProc();
    else 
        throw L"Module " + m_name + L":: No DestroyModule symbol.";
}

void ModuleDescriptor::SubmitData(void* data, size_t dataSz)
{
    if (m_submitDataProc)
        m_submitDataProc(data, dataSz);
    else
        throw L"Module " + m_name + L":: No SubmitData symbol.";
}

void ModuleDescriptor::AssociateDbInstance(DbInstance db)
{
    m_dbInstances.push_back(db);
}

std::list<DbInstance>& ModuleDescriptor::GetDbInstances()
{
    return m_dbInstances;
}

} // namespace cuc