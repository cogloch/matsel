#pragma once
#include "gui/mfcpp.hpp"
#include "api/module.hpp"
#include "db_instance.hpp"
#include "resource_descriptor.hpp"


// TODO: Fix releasing module handles 

namespace cuc
{

class ModuleDescriptor : public ResourceDescriptor
{
public:
    ModuleDescriptor();
    ModuleDescriptor(const std::wstring& path);
    ~ModuleDescriptor();

    //ModuleDescriptor(ModuleDescriptor&&) = delete;
    //ModuleDescriptor& operator=(ModuleDescriptor&&) = delete;

    ModuleDescriptor(ModuleDescriptor&&) = default;
    ModuleDescriptor& operator=(ModuleDescriptor&&) = default;

    ModuleDescriptor(const ModuleDescriptor&) = delete;
    ModuleDescriptor& operator=(const ModuleDescriptor&) = delete;

    const std::wstring& GetName() const;
    bool Load();
    HMODULE GetHandle();

    // Returns a list of symbols (by index in the required vector) that were not found 
    std::vector<size_t> LoadSymbols(const std::vector<std::string>& required);
    std::vector<std::string> GetExtraSymbols(const std::vector<std::string>& required);

    // API 
    void CreateModule(CreateModuleArgs*); 
    void DestroyModule();
    void SubmitData(void* data, size_t dataSz);

    // Persistent data
    void AssociateDbInstance(DbInstance);
    std::list<DbInstance>& GetDbInstances();
    
    // Common to all associated DBs
    // Takes the module's built Schema (in the API, module.hpp), adds cols common to all modules, and uses STL 
    CoreSchema m_coreSchema;
    bool m_needSpecial = true; 
    // TODO: handle the special fields nicer 
    // TODO: init m_coreSchema in ctr 

    void InitModuleMetadata(/*std::string name, */std::string description, Schema*);

    const DbInstance* GetDbInstanceByName(const std::wstring& name) const;

private:
    size_t m_mySize;

    std::wstring m_name;
    std::wstring m_updateTimestamp;
    std::wstring m_description;
    HMODULE m_winHandle;

    // API mapping
    std::function<void(CreateModuleArgs*)> m_createModuleProc;
    std::function<void()> m_destroyModuleProc;
    std::function<void(void*, size_t)> m_submitDataProc;

    // Persistent data 
    //std::vector<ModuleDbDescriptor> m_dbDescs;
    std::list<DbInstance> m_dbInstances;
};

} // namespace cuc