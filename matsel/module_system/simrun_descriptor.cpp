#include "pch.hpp"
#include "simrun_descriptor.hpp"


namespace cuc
{

ModuleInstance::ModuleInstance(ModuleDescriptor& desc, DbInstance& db)
    : m_desc(desc)
    , m_activeDb(db)
{
}

void SimrunDescriptor::Run()
{

}

void SimrunDescriptor::Pause()
{

}

void SimrunDescriptor::Stop()
{

}

void SimrunDescriptor::Load(const std::wstring& path)
{
    UNREFERENCED_PARAMETER(path);
}

} // namespace cuc