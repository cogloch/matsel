#include "pch.hpp"
#include "common/files.hpp"
#include "module_manager.hpp"
#include "common/common_types.hpp"


namespace cuc
{

void ModuleManager::Serialize()
{
    std::wofstream file(GetDataPath() + L"matsel_modules_cache");
    for (auto& mod : m_loadedModules)
    {
        file << mod.GetPath() << L"\n";
        auto dbs = mod.GetDbInstances();
        file << dbs.size() << L"\n";
        for (const auto& dbPath : dbs)
            file << dbPath.GetPath() << L"\n";
    }
}

std::vector<std::wstring> ModuleManager::Deserialize()
{
    std::vector<std::wstring> loadedNames;
    const auto cachePath = GetDataPath() + L"matsel_modules_cache";
    const auto cachePathTemp = cachePath + L"_temp";

    if (!std::filesystem::exists(cachePath))
        return loadedNames;
    if (std::filesystem::exists(cachePathTemp))
        std::filesystem::remove(cachePathTemp);
    std::filesystem::copy_file(cachePath, cachePathTemp);

    std::wifstream file(cachePathTemp);
    for (std::wstring path; std::getline(file, path);)
    {
        if (path.length() < 3) continue;

        bool ok = true;
        try
        {
            Load(path);
            loadedNames.push_back(m_loadedModules.back().GetName());
        }
        catch (const std::runtime_error& err)
        {
            ErrorMessageBox(err.what());
            ok = false;
        }

        std::wstring token; 
        std::getline(file, token);
        auto numDbs = std::stoi(token);

        for (int dbIdx = 0; dbIdx < numDbs; ++dbIdx)
        {
            std::wstring dbPath;
            std::getline(file, dbPath);
            if (ok)
            {
                LoadDb(m_loadedModules.size() - 1, dbPath, true);
            }
        }
    }

    Serialize();

    return loadedNames;
}


void ModuleManager::AddDbDesc(size_t moduleIdx, DbInstance db)
{
    GetModule(moduleIdx).AssociateDbInstance(db);
    Serialize();
}

std::list<DbInstance>& ModuleManager::GetDbDescs(size_t moduleIdx)
{
    return GetModule(moduleIdx).GetDbInstances();
}

ModuleDescriptor& ModuleManager::GetModule(size_t idx)
{
    return *std::next(m_loadedModules.begin(), idx);
}

std::optional<size_t> ModuleManager::GetIndexByName(const std::wstring& name)
{
    for (size_t i = 0, numModules = m_loadedModules.size(); i < numModules; ++i)
        if (GetModule(i).GetName() == name)
            return i;
    return {};
}

void ModuleManager::UpdateByName(const std::wstring& name)
{
    std::wstring oldPath;
    for (auto it = m_loadedModules.begin(); it != m_loadedModules.end(); ++it)
        if (it->GetName() == name)
        {
            oldPath = it->GetPath();
            m_loadedModules.erase(it);
            break;
        }

    Load(oldPath);
}

void ModuleManager::Unload(const std::wstring& path)
{
    auto idx = GetIndexByName(path);
    if (idx)
    {
        m_loadedModules.erase(std::next(m_loadedModules.begin(), *idx));
        Serialize();
    }
}

const ModuleDescriptor* ModuleManager::GetModulePtrByName(const std::wstring& name) const
{
    for (auto it = m_loadedModules.begin(); it != m_loadedModules.end(); ++it)
        if (it->GetName() == name)
            return &*it;
    return nullptr;
}

void ModuleManager::SolveReference(DataReference& ref) const
{
    auto mod = GetModulePtrByName(ref.mod);
    if (!mod) return;

    auto db = mod->GetDbInstanceByName(ref.db);
    if (!db) return;

    auto value = db->GetValueByKey(ref.key, ref.field);
    if (!value.empty())
    {
        ref.value = new wchar_t[value.size() + 1];
        wcscpy(ref.value, value.c_str());
    }
}

void CleanModuleCreateArgs(CreateModuleArgs& args)
{
#ifndef _DEBUG
    FreeCString(&args.name);
    FreeCString(&args.description);
    
    delete[] args.schema->types;
    args.schema->types = nullptr;
    for (size_t i = 0; i < args.schema->numLabels; ++i)
        delete[] args.schema->labels[i];
    delete[] args.schema->labels;
    args.schema->labels = nullptr;
    args.schema->numLabels = 0;
#endif
}
//test

void AddSpecialToSchema(CoreSchema& schema)
{
    static const std::string common[] = {
        "source",        // simrun_name.key or manual
        "ts_wall",       // wall timestamp
        "ts_pointer"     // simrun "timestamp" 
                         //         - not necessarily time, but a pointer in the log pointed to by @source
    };

    for (auto& col : common)
        schema.push_back({ col, SchemaType::String });

    static const std::map<SchemaType, std::string> typeMap = {
        { SchemaType::String, "text" },
        { SchemaType::Integer, "int" },
        { SchemaType::Real, "real" },
    };

    for (auto& col : schema)
        col.first += "$" + typeMap.at(col.second);
}

const DbInstance* ModuleManager::LoadDb(size_t moduleIdx, const std::wstring& path, bool mustExist)
{
    auto exists = std::filesystem::exists(path);
    bool creating = false;
    if (!exists)
    {
        if (mustExist)
            return nullptr;
        else
            creating = true;
    }

    if (exists && !mustExist)
    {
        std::filesystem::remove(path);
        creating = true;
    }

    auto& mod = GetModule(moduleIdx);
    auto& dbs = mod.GetDbInstances();

    // Check if loaded 
    for (auto& db : dbs)
        if (db.GetPath() == path)
            //return &db;
            return nullptr;

    auto& schema = mod.m_coreSchema;
    // Merge special and specific cols
    if (mod.m_needSpecial)
    {
        AddSpecialToSchema(schema);
        mod.m_needSpecial = false;
    }

    DbInstance db(path, schema);
    db.ConnectSql();
    db.BeginTransaction();
    if (!creating) // Check if existing DB is valid 
    {
        try 
        {
            std::vector<std::string> dummy;
            for (size_t i = 0; i < schema.size(); ++i)
                dummy.push_back("schema_test");
            db.InsertRow(dummy);

            // Test ok! Clean up
            db.DeleteRow(db.db->last_insert_rowid());
        } 
        catch (...) 
        {
            return nullptr;
        }
    }
    else // Create DB 
    {
        db.CreateMainTable();
    }
    db.EndTransaction();

    mod.AssociateDbInstance(db);
    Serialize();

    return &mod.GetDbInstances().back(); // This is incredibly bad
}

void ModuleManager::Load(const std::wstring& path)
{
    // TODO: handle name conflicts
    // TODO: validate module
    // TODO: Hot-loading 

    ModuleDescriptor newMod(path);
    const std::string pathA(WtoA(path));

    if (IsLoaded(newMod.GetPath()))
        throw std::runtime_error("Loading module " + pathA + " failed: already loaded.");

    if (!newMod.Load())
        throw std::runtime_error("Loading module " + pathA + " failed: WinAPI LoadLibrary() returned code " + std::to_string(GetLastError()));
    
    const std::vector<std::string> requiredAPI = {
        #ifdef FREE_API
        "CreateModule",
        "DestroyModule",
        "SubmitData"
        #endif
    };

    auto missingSymbols = newMod.LoadSymbols(requiredAPI);
    auto extraSymbols = newMod.GetExtraSymbols(requiredAPI);

    if (!missingSymbols.empty() || !extraSymbols.empty())
    {
        std::string errMsg;
        if (!missingSymbols.empty())
        {
            errMsg += "Could not load symbol(s): \n";
            for (const auto& missingIdx : missingSymbols)
                errMsg += requiredAPI[missingIdx] + "\n";
        }

        if(!extraSymbols.empty())
        {
            errMsg += "Found extra symbol(s): \n";
            for (const auto& extra : extraSymbols)
                errMsg += extra + "\n";
        }

        const auto nameA = CStringA(newMod.GetName().c_str());
        MessageBoxA(nullptr, errMsg.c_str(), "Failed loading: " + nameA, MB_ICONERROR);
    }

    if (!missingSymbols.empty())
    {
        std::string missingList;
        for (const auto& symb : missingSymbols)
            missingList += symb + ", ";
        missingList.pop_back(); missingList.pop_back();
        throw std::runtime_error("Loading module " + pathA + " failed: mssing symbols - " + missingList);
    }

    CreateModuleArgs args = {};
    newMod.CreateModule(&args);
    // Deal with CreateModule() output from loaded module
    newMod.InitModuleMetadata(args.description, args.schema);
    // Store     
    m_loadedModules.push_back(std::move(newMod));
    Serialize();
    
    CleanModuleCreateArgs(args);
}

bool ModuleManager::IsLoaded(const std::wstring& path)
{
    for (const auto& existing : m_loadedModules)
        if (existing.GetPath() == path)
            return true;
    return false;

    // TODO: Alternatively, check for GetModuleHandle(path) 
}

} // namespace cuc 