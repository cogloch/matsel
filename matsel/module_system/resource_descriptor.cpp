#include "pch.hpp"
#include "resource_descriptor.hpp"
#include "gui/mfcpp.hpp"


namespace cuc
{

const std::wstring& ResourceDescriptor::GetPath() const
{
    return m_path;
}

std::string ResourceDescriptor::GetPathA() const
{
    return WtoA(m_path);
}

} // namespace cuc 