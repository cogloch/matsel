#pragma once
#include "db_instance.hpp"
#include "module_descriptor.hpp"


namespace cuc
{

// A stage in the simrun, representing a module.
// There may be multiple different instances (stages) for a single module. 
// No two module instances may use the same db at the same time. (??)
// TODO: rename to "Stage"; have special stages that can be part of the simrun, but are not proper modules?  
class ModuleInstance
{
public:
    ModuleInstance(ModuleDescriptor&, DbInstance&);

    struct Settings
    {
        // ... 
    } m_settings;     

    // TODO: this is utter rubbish
    ModuleDescriptor& m_desc;
    DbInstance& m_activeDb;
};

// TODO: disable manual edits for active DBs (part of a running simrun)
// TODO: check for active DB conflicts 

// Either module-like DLL, or plain data ("script")
// TODO: store timeline 
// TODO: resource pooling/arbitration
// TODO: gui 
class SimrunDescriptor
{
public:
    void Run();
    void Pause();
    void Stop();

    void Load(const std::wstring& path);

    std::wstring m_name;
    std::vector<ModuleInstance> m_modules;
};

} // namespace cu