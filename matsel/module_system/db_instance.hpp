#pragma once
#include "resource_descriptor.hpp"
#include "common/common_types.hpp"
#include "api/module.hpp"


// TODO: Clean up keys, redirect references 
//             -> instance keeps a mapping of the key history as they are cleaned up multiple times, such that 
//                a referenced key in a different db won't have to change; it will just be redirected to the newest corresponding key 

namespace cuc
{

using CoreSchema = std::vector<std::pair<std::string, SchemaType>>;

class DbInstance : public ResourceDescriptor
{
public:
    DbInstance(const CString& path, CoreSchema&);
    DbInstance(const std::wstring& path, CoreSchema&);
    ~DbInstance();

    sqlite::database& Get() { return *db; }
    const std::wstring& GetName() const;
    const std::string& GetMainTable() const { return m_mainTable; }

    //std::shared_ptr<sqlite::database> db;
    sqlite::database* db;
    CoreSchema& m_coreSchema;               // Actual resource in ModuleDescriptor, such that the schema 
                                            // is common to all DBs associated with a single module 

    void ConnectSql();

    void CreateMainTable();
    void InsertRow(std::vector<std::string>& vals);
    void UpdateRow(i64 key, std::vector<std::string>& vals);
    void DeleteRow(i64 key);
    void BeginTransaction();
    void EndTransaction();
    void NullifyRow(i64 key);
    int CountRows();
    std::vector<int> GetNonNullKeys();
    std::wstring GetValueByKey(i64 key, std::wstring col) const;

private:
    std::wstring m_name;
    std::string m_mainTable = "mat";

    // TODO: share between all instances of a single module (i.e. as in CoreSchema)
    // TODO: array 
    sqlite::database_binder* m_psCountRows = nullptr;
    sqlite::database_binder* m_psDeleteRow = nullptr;
    sqlite::database_binder* m_psUpdateRow = nullptr;
    sqlite::database_binder* m_psNullifyRow = nullptr;
    sqlite::database_binder* m_psInsertFullRow = nullptr;
    sqlite::database_binder* m_psGetNonNullKeys = nullptr;

    // One for each col 
    //std::vector<sqlite::database_binder*> m_psGetColByKeys;
};

} // namespace cuc 