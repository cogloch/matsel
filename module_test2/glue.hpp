#pragma once
#define MODULE
#include "matsel/api/module.hpp"


MATSEL void CreateModule(CreateModuleArgs*);
MATSEL void DestroyModule();
MATSEL void SubmitData(void* data, size_t sz);