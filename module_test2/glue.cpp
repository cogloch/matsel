#include "glue.hpp"


MATSEL void CreateModule(CreateModuleArgs* args)
{
    args->name = AllocCString("Test Module #2");
    args->description = AllocCString("Description of test module 2. Lots of test stuff. ddlll");

    args->schema = new Schema;
    args->schema->numLabels = 2;

    args->schema->types = new SchemaType[args->schema->numLabels];
    args->schema->types[0] = SchemaType::String;
    args->schema->types[1] = SchemaType::Integer;

    args->schema->labels = new char* [args->schema->numLabels];
    args->schema->labels[0] = AllocCString("TestProp2");
    args->schema->labels[1] = AllocCString("AnotherPropPlus");
}

MATSEL void DestroyModule()
{

}

MATSEL void SubmitData(void* data, size_t sz)
{
    
}